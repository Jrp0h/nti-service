import express from "express";

import CommentsController from "../../Controllers/CommentsController";
import DFM from "../../Middlewares/DocumentFetcherMiddleware";

let router = express.Router();

router.get("/", CommentsController.Index);
router.post("/", CommentsController.Store);

router.delete("/:cid", DFM.FetchComment, CommentsController.Destroy);

router.get("/:cid", DFM.FetchComment, CommentsController.Index);
router.post("/:cid", DFM.FetchComment, CommentsController.Store);

export default router;
