import express from "express";

import ProblemsController from "../../Controllers/ProblemsController";
import AuthGuard from "../../Guards/AuthGuard";
import DFM from "../../Middlewares/DocumentFetcherMiddleware";
import Paginate from "../../Middlewares/PaginationMiddleware";

let router = express.Router();

router.get("/", AuthGuard.AdminRequired, Paginate.Use, ProblemsController.Index);
router.post("/", ProblemsController.Store);

router.get("/:pid", DFM.FetchProblem, AuthGuard.AdminOrOwnProblem, ProblemsController.Show);
router.patch("/:pid", DFM.FetchProblem, AuthGuard.AdminOrOwnProblem, ProblemsController.Update);
router.delete("/:pid", DFM.FetchProblem, AuthGuard.AdminOrOwnProblem, ProblemsController.Destroy);

// ADMIN ONLY ROUTES
router.patch("/:pid/assign", AuthGuard.AdminRequired, DFM.FetchProblem, ProblemsController.Assign);
router.delete("/:pid/assign", AuthGuard.AdminRequired, DFM.FetchProblem, ProblemsController.RemoveAssign);

router.patch("/:pid/waiting", AuthGuard.AdminRequired, DFM.FetchProblem, ProblemsController.Mark("waiting"));
router.patch("/:pid/inprogress", AuthGuard.AdminRequired, DFM.FetchProblem, ProblemsController.Mark("in progress"));
router.patch("/:pid/done", AuthGuard.AdminRequired, DFM.FetchProblem, ProblemsController.Mark("done"));

import CommentRoutes from "./CommentRoutes";
router.use("/:pid/comments", DFM.FetchProblem, AuthGuard.AdminOrOwnProblem, CommentRoutes);

export default router;
