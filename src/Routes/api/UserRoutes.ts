import express from "express";

import UsersController from "../../Controllers/UsersController";
import AuthGuard from "../../Guards/AuthGuard";
import Paginate from "../../Middlewares/PaginationMiddleware";

let router = express.Router();

router.post("/login", UsersController.Login);
router.post("/register", UsersController.Register);
router.get("/logout", AuthGuard.Required, UsersController.Logout);
router.get("/logout/all", AuthGuard.Required, UsersController.LogoutAll);

// ADMIN ONLY ROUTES
router.post("/", AuthGuard.AdminRequired, Paginate.Use, UsersController.Index);
router.patch("/:id/ban", AuthGuard.AdminRequired, UsersController.Ban(true));
router.patch("/:id/unban", AuthGuard.AdminRequired, UsersController.Ban(false));

export default router;
