import express from "express";

import CategoriesController from "../../Controllers/CategoriesController";
import AuthGuard from "../../Guards/AuthGuard";

let router = express.Router();

router.get("/", AuthGuard.Required, CategoriesController.Index);
router.post("/", AuthGuard.AdminRequired, CategoriesController.Store);

router.get("/:id", AuthGuard.Required, CategoriesController.Show);
router.patch("/:id", AuthGuard.AdminRequired, CategoriesController.Update);

export default router;
