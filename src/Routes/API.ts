import express from "express";
import AuthGuard from "../Guards/AuthGuard";

let router: express.Router = express.Router();

import UserRoutes from './api/UserRoutes';
router.use("/auth", UserRoutes);

import ProblemRoutes from './api/ProblemRoutes';
router.use("/problems", AuthGuard.Required, ProblemRoutes);

import CategoryRoutes from './api/CategoryRoutes';
router.use("/categories", CategoryRoutes);


export default router;

