import ValidationError from "../Errors/ValidationError";
import Str from "./Str";

export default class ErrorHandeling {

   static Handle(error: any) {
      console.log(error.code);
      if (error.code === 11000) {

         let errors = new Array<ValidationError>();

         for (let key in error.keyValue) {
            let vError = new ValidationError(Str.Capitalize(key) + " is already taken", key, "db.unique");
            vError.title = "Duplicate Key";
            errors.push(vError);
         }

         return {errors};
      }

      console.error(error);
      return {error: error.message};
      // throw new Error("Undefined error without handler");
   }
}
