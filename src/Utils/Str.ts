export default class Str {

   static Capitalize(text: string): string {
      return text.substr(0, 1).toUpperCase() + text.slice(1);
   }

}

