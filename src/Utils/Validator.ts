import Joi from "joi";
import ValidationError from "../Errors/ValidationError";
import Str from "./Str";

export default class Validator {

   static Validate(schema: Joi.ObjectSchema<any>, data: any) {
      const {error} = schema.validate(data);

      if (error) {
         return this._HandleError(error);
      }
      else {
         return true;
      }
   }

   private static _HandleError(error: Joi.ValidationError) {

      let errors = new Array<ValidationError>();

      for (let detail of error.details) {

         let vError = new ValidationError("", detail.type, detail.context!.key!);

         switch (detail.type) {
            case "any.required":
               vError.detail = Str.Capitalize(detail.context!.key + " is required");
               break;
            case "object.unknown":
               vError.detail = Str.Capitalize(detail.context!.key + " is not allowed");
               break;
            case "string.min":
               vError.detail = Str.Capitalize(detail.context!.key + " must be at least " + Validator._GetLimit(detail) + " characters");
               break;
            case "string.max":
               vError.detail = Str.Capitalize(detail.context!.key + " must be less than or equal to " + Validator._GetLimit(detail) + " characters");
               break;
            case "string.length":
               vError.detail = Str.Capitalize(detail.context!.key + " must be " + Validator._GetLimit(detail) + " characters");
               break;
            case "string.email":
               vError.detail = Str.Capitalize(detail.context!.key + " is invalid");
               break;
            case "string.hex":
               vError.detail = Str.Capitalize(detail.context!.key + " must be a hex value");
               break;
            case "string.pattern.base":
               vError.detail = Str.Capitalize(detail.context!.key + " is invalid");
               break;
            default:
               console.error(detail);
               throw new Error("Unknown " + detail.type);
         }

         errors.push(vError);
      }

      return {
         errors
      }
   }

   static IsValidObjectId(_id: string): boolean {

      const schema = Joi.object({
         _id: Joi.string().hex().length(24).required()
      });

      let validated = Validator.Validate(schema, {_id});

      if (validated !== true)
         return false;

      return true;
   }

   private static _GetLimit(obj: any) {
      return obj.context.limit;
   }
}
