import * as jwt from "jsonwebtoken";

export default class Token {

   static Sign(data: any) {
      if (process.env.JWT_SECRET == undefined)
         throw "Missing JWT Secret!";

      // One Week
      return jwt.sign(data, process.env.JWT_SECRET, {expiresIn: (7 * 24 * 60 * 60)});
   }

   static Verify(token: string) {
      if (process.env.JWT_SECRET == undefined)
         throw "Missing JWT Secret!";

      try {
         return jwt.verify(token, process.env.JWT_SECRET);
      } catch (error) {
         return false;
      }
   }
}
