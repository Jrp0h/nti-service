import Comment, {IComment} from "../Models/Comment";
import User from "../Models/User";

export default class CommentConstructor {

   static async Construct(comment: IComment) {

      let _poster = await User.findOne({_id: comment._poster});

      let comments = await Comment.find({_on: comment._id}).sort({created_at: -1});

      let cOnC: any = new Array<any>();

      for (let c of comments) {
         cOnC.push(this.Construct(c));
      }

      if (cOnC.length <= 0) cOnC = null;

      return {
         _id: comment._id,
         _on: comment._on,
         body: comment.body,
         created_at: comment.created_at,
         updated_at: comment.updated_at,
         poster: {
            _id: _poster!._id,
            name: _poster!.name
         },
         comments: cOnC
      };
   }
}
