import {IProblem} from "../Models/Problem";
import User from "../Models/User";
import Comment from "../Models/Comment";
import CommentConstructor from "./CommentConstructor";
import Category from "../Models/Category";


export default class ProblemConstructor {

   static async Construct(problem: IProblem) {

      let _requestor = await User.findOne({_id: problem._requestor});
      let _fixer = await User.findOne({_id: problem._fixer});
      let _category = await Category.findOne({_id: problem._category});

      let fixerObject: any = {};

      if (_fixer == undefined)
         fixerObject = null;
      else
         fixerObject = {
            _id: _fixer._id,
            name: _fixer.name
         };

      return {
         _id: problem._id,
         title: problem.title,
         description: problem.description,
         status: problem.status,
         created_at: problem.created_at,
         updated_at: problem.updated_at,
         category: {
            _id: _category!._id,
            name: _category!.name,
         },
         requestor: {
            _id: _requestor!._id,
            name: _requestor!.name
         },
         fixer: fixerObject
      };
   }

   static async ConstructFull(problem: IProblem) {

      let _requestor = await User.findOne({_id: problem._requestor});
      let _fixer = await User.findOne({_id: problem._fixer});

      let fixerObject: any = {};

      if (_fixer == undefined)
         fixerObject = null;
      else
         fixerObject = {
            _id: _fixer._id,
            name: _fixer.name
         };

      let _comments = await Comment.find({_on: problem._id}).sort({created_at: -1});

      let allComments = new Array<any>();

      for (let comment of _comments) {
         allComments.push(await CommentConstructor.Construct(comment));
      }

      return {
         _id: problem._id,
         title: problem.title,
         description: problem.description,
         status: problem.status,
         created_at: problem.created_at,
         requestor: {
            _id: _requestor!._id,
            name: _requestor!.name
         },
         fixer: fixerObject,
         comments: allComments
      };
   }
}
