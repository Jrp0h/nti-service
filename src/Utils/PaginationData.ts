export default class PaginationData {

   total_enteries: number | undefined | null;
   next: IPaginateData | undefined | null;
   previous: IPaginateData | undefined | null;
   start: IPaginateData | undefined | null;
   current: IPaginateData | undefined | null;
   end: IPaginateData | undefined | null;

   static Generate(currentPage: number, limit: number, totalAmount: number): PaginationData {

      const startIndex = (currentPage - 1) * limit;
      const endIndex = currentPage * limit;

      let data = new PaginationData();

      if (endIndex < totalAmount)
         data.next = {page: currentPage + 1, limit};

      if (startIndex > 0)
         data.previous = {page: currentPage - 1, limit};

      data.start = {
         page: 1,
         limit
      };

      data.current = {
         page: currentPage,
         limit
      };

      data.end = {
         page: Math.ceil(totalAmount / limit),
         limit
      };

      data.total_enteries = totalAmount;

      return data;
   }

}

interface IPaginateData {
   page: number;
   limit: number;
}
