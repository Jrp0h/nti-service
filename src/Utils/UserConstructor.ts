import {IUser} from "../Models/User";

export default class UserConstructor {

   static Construct(user: IUser) {
      return {
         _id: user._id,
         email: user.email,
         permissions: user.permissions,
         banned: user.banned,
         create_at: user.created_at,
         updated_at: user.updated_at,
      }
   }

}
