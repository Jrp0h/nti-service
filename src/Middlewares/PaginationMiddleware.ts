import {Request, Response, NextFunction} from 'express';
import {ValidationError} from 'joi';

export default class Paginate {
   static async Use(req: Request, res: Response, next: NextFunction) {

      let pagination: {
         page: number,
         limit: number,
         skip: number
      } = {page: 1, limit: 25, skip: 25};

      try {
         if (req.query.page !== undefined)
            pagination.page = +req.query.page;

         if (pagination.page < 1)
            pagination.page = 1;
      } catch (error) {
         return res.status(400).json({errors: [new ValidationError("Page is not a number", "query.page", "number.invalid")]});
      }

      try {
         if (req.query.limit !== undefined)
            pagination.limit = +req.query.limit;

         if (pagination.limit < 1)
            pagination.limit = 1;
      } catch (error) {
         return res.status(400).json({errors: [new ValidationError("Limit is not a number", "query.limit", "number.invalid")]});
      }

      pagination.skip = (pagination.page - 1) * pagination.limit;

      res.locals.pagination = pagination;

      next();
   }
}

