import express, {Request, Response, NextFunction} from 'express';
import Problem from '../Models/Problem';
import Comment from '../Models/Comment';

export default class DocumentFetcherMiddleware {

   static async FetchProblem(req: Request, res: Response, next: NextFunction) {

      const {pid} = req.params;

      try {
         let problem = await Problem.findOne({_id: pid});

         if (problem == undefined)
            return res.status(404).json({error: {status: 404, message: "Problem with id " + pid + " not found"}});

         res.locals.problem = problem;

         next();
      } catch (error) {
         return res.status(404).json({error: {status: 404, message: "Problem with id " + pid + " not found"}});
      }
   }

   static async FetchComment(req: Request, res: Response, next: NextFunction) {

      const {cid} = req.params;

      try {
         let comment = await Comment.findOne({_id: cid});

         if (comment == undefined)
            return res.status(404).json({error: {status: 404, message: "Comment with id " + cid + " not found"}});

         res.locals.comment = comment;

         next();
      } catch (error) {
         return res.status(404).json({error: {status: 404, message: "Comment with id " + cid + " not found"}});
      }
   }

}
