import mongoose, {Schema, Document} from "mongoose";

export interface ICategory extends Document {
   _id: mongoose.Types.ObjectId;
   name: String;
   created_at?: Date;
   updated_at?: Date;
}

const CategorySchema: Schema = new Schema({
   _id: Schema.Types.ObjectId,
   name: {
      type: String,
      required: true
   },
   created_at: {
      type: Date,
      default: Date.now
   },
   updated_at: {
      type: Date,
      default: Date.now
   }
}, {versionKey: false});

CategorySchema.pre<ICategory>("save", function (next: any) {
   if (this._id == undefined) this._id = new mongoose.Types.ObjectId();

   this.updated_at = new Date();

   next();
});

export default mongoose.model<ICategory>("Category", CategorySchema);
