import mongoose, {Schema, Document} from "mongoose";
import Comment from "./Comment";

export interface IProblem extends Document {
   _id: mongoose.Types.ObjectId;
   _requestor: mongoose.Types.ObjectId;
   _category: mongoose.Types.ObjectId;
   _fixer?: mongoose.Types.ObjectId;
   title: string;
   description: string;
   status?: string;
   created_at?: Date;
   updated_at?: Date;
}

const ProblemSchema: Schema = new Schema({
   _id: Schema.Types.ObjectId,
   _requestor: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true
   },
   _fixer: {
      type: Schema.Types.ObjectId,
      ref: "User",
      default: null
   },
   _category: {
      type: Schema.Types.ObjectId,
      ref: "Category"
   },
   title: String,
   description: String,
   status: {
      type: String,
      default: "waiting"
   },
   created_at: {
      type: Date,
      default: Date.now
   },
   updated_at: {
      type: Date,
      default: Date.now
   }
}, {versionKey: false});

ProblemSchema.pre<IProblem>("save", function (next: any) {
   if (this._id == undefined) this._id = new mongoose.Types.ObjectId();

   this.updated_at = new Date();

   next();
});

ProblemSchema.pre<IProblem>("remove", function (next: any) {

   Comment.find({_on: this._id}).exec((error, comments) => {
      comments.forEach(comment => {
         comment.remove();
      });
   });

   next();
});

export default mongoose.model<IProblem>("Problem", ProblemSchema);
