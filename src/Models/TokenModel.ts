import mongoose, {Schema, Document} from "mongoose";

export interface IToken extends Document {
   _id: mongoose.Types.ObjectId;
   _user_id: mongoose.Types.ObjectId;
   token: String;
   created_at?: Date;
   updated_at?: Date;
   expires_at?: Date;
}

const TokenSchema: Schema = new Schema({
   _id: Schema.Types.ObjectId,
   _user_id: Schema.Types.ObjectId,
   token: String,
   created_at: {
      type: Date,
      default: Date.now
   },
   updated_at: {
      type: Date,
      default: Date.now
   },
   expires_at: {
      type: Date,
      default: Date.now,
      expires: 0
   }
}, {versionKey: false});

TokenSchema.pre<IToken>("save", function (next: any) {
   if (this._id == undefined) this._id = new mongoose.Types.ObjectId();

   this.expires_at = new Date(Date.now() + (7 * 24 * 60 * 60));

   next();
});

export default mongoose.model<IToken>("Token", TokenSchema);
