import mongoose, {Schema, Document} from "mongoose";

export interface IUser extends Document {
   _id: mongoose.Types.ObjectId;
   name: string;
   email: string;
   password: string;
   permissions?: number;
   created_at?: Date;
   updated_at?: Date;
   banned?: boolean;
}

const UserSchema: Schema = new Schema({
   _id: Schema.Types.ObjectId,
   name: String,
   email: {
      type: String,
      unique: true
   },
   password: {
      type: String,
      select: false
   },
   permissions: {
      type: Number,
      default: 0
   },
   banned: {
      type: Boolean,
      default: false
   },
   created_at: {
      type: Date,
      default: Date.now
   },
   updated_at: {
      type: Date,
      default: Date.now
   }
}, {versionKey: false});

UserSchema.pre<IUser>("save", function (next: any) {
   if (this._id == undefined) this._id = new mongoose.Types.ObjectId();

   this.updated_at = new Date();

   next();
});

export default mongoose.model<IUser>("User", UserSchema);
