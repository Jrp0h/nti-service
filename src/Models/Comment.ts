import mongoose, {Schema, Document} from "mongoose";

export interface IComment extends Document {
   _id: mongoose.Types.ObjectId;
   _poster: mongoose.Types.ObjectId;
   _on: mongoose.Types.ObjectId;
   body: string,
   created_at?: Date;
   updated_at?: Date;
}

const CommentSchema: Schema = new Schema({
   _id: Schema.Types.ObjectId,
   _poster: {
      type: Schema.Types.ObjectId,
      ref: "User"
   },
   _on: {
      type: Schema.Types.ObjectId,
      ref: "Problem",
      required: true
   },
   body: {
      type: String,
      required: true
   },
   created_at: {
      type: Date,
      default: Date.now
   },
   updated_at: {
      type: Date,
      default: Date.now
   }
}, {versionKey: false});

CommentSchema.pre<IComment>("save", function (next: any) {
   if (this._id == undefined) this._id = new mongoose.Types.ObjectId();

   this.updated_at = new Date();

   next();
});

export default mongoose.model<IComment>("Comment", CommentSchema);
