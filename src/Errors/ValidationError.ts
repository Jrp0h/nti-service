export default class ValidationError {

   detail: string;
   title: string;
   field: string;
   type: string;

   constructor(detail: string, field: string, type: string) {
      this.title = "Invalid Attribute";
      this.detail = detail;
      this.field = field;
      this.type = type;
   }

}
