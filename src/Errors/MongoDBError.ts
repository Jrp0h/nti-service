export default class MongoDBError {

   code: number;
   title: string;
   pairs: Array<{
      key: string;
      value: string;
   }>;

   constructor(code: number, title: string, pairs: Array<{key: string, value: string}>) {
      this.code = code;
      this.title = title;
      this.pairs = pairs;
   }

}
