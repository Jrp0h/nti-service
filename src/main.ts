import express, {Request, Response} from "express";
import mongoose from "mongoose";

import * as dotenv from "dotenv";
dotenv.config();

mongoose.connect(
   <string>process.env.MONGODB_URI,
   {useNewUrlParser: true, useUnifiedTopology: true},
   (err: any) => {
      if (err) {
         process.exit(1);
      }
   }
);

const app: express.Application = express();

app.use(express.json());
app.use(
   express.urlencoded({
      extended: true
   })
);

import API from "./Routes/API";

app.use("/api", API);

app.listen(process.env.PORT, () => console.log(`Server running on port ${process.env.PORT}`));
