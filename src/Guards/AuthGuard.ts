import {Request, Response, NextFunction} from 'express';
import TokenModel from '../Models/TokenModel';
import Token from '../Utils/Token';

export default class AuthGuard {

   static async Required(req: Request, res: Response, next: NextFunction) {
      if (req.headers.authorization == undefined)
         return res.status(401).json({error: "Unauthorized"});

      let tokenString: string;

      try {
         tokenString = req.headers.authorization.split(" ")[1];
      } catch (error) {
         return res.status(401).json({error: "Unauthorized"});
      }

      let token: any = Token.Verify(tokenString);

      if (token === false)
         return res.status(401).json({error: "Unauthorized"});

      let exists = await TokenModel.findOne({_user_id: token.user._id, token: tokenString});

      if (exists == undefined)
         return res.status(401).json({error: "Unauthorized"});

      res.locals.user = {
         _id: token.user._id,
         permissions: token.user.permissions,
         isAdmin: token.user.permissions === 1
      };

      res.locals.token = tokenString;

      next();
   }

   static async AdminRequired(req: Request, res: Response, next: NextFunction) {

      // Prevent double token verification
      if (res.locals.user !== undefined) {
         if (res.locals.user.isAdmin)
            return next();
         else
            return res.status(403).json({error: "Forbidden"});
      }

      if (req.headers.authorization == undefined)
         return res.status(401).json({error: "Unauthorized"});

      let tokenString: string;

      try {
         tokenString = req.headers.authorization.split(" ")[1];
      } catch (error) {
         return res.status(401).json({error: "Unauthorized"});
      }

      let token: any = Token.Verify(tokenString);

      if (token === false)
         return res.status(401).json({error: "Unauthorized"});

      if (token.user.permissions !== 1)
         return res.status(403).json({error: "Forbidden"});

      let exists = await TokenModel.findOne({_user_id: token.user._id, token: tokenString});

      if (exists == undefined)
         return res.status(401).json({error: "Unauthorized"});

      res.locals.user = {
         _id: token.user._id,
         permissions: token.user.permissions,
         isAdmin: token.user.permissions === 1
      };

      res.locals.token = tokenString;

      next();
   }

   static async AdminOrOwnProblem(req: Request, res: Response, next: NextFunction) {
      if (res.locals.problem._id != res.locals.user._id && !res.locals.user.isAdmin)
         return res.status(403).json({error: "Forbidden"});

      next();
   }
}

