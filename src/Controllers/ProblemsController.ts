import {Request, Response} from "express";
import Joi from "joi";
import ValidationError from "../Errors/ValidationError";
import Category from "../Models/Category";

import Problem, {IProblem} from "../Models/Problem";
import ErrorHandeling from "../Utils/ErrorHandeling";
import PaginationData from "../Utils/PaginationData";
import ProblemConstructor from "../Utils/ProblemConstructor";
import Validator from "../Utils/Validator";

export default class ProblemsController {

   static async Index(req: Request, res: Response) {

      try {
         let searchQuery: any;

         // Find all not marked as done
         if (res.locals.user.isAdmin)
            searchQuery = {status: {$ne: "done"}};
         else
            searchQuery = {status: {$ne: "done"}, _requestor: res.locals.user._id};

         if (req.query.category !== undefined) {
            if (Validator.IsValidObjectId(<string>req.query.category))
               searchQuery._category = req.query.category;
            else
               return res.status(400).json({errors: [new ValidationError("Category is an invalid category_id", "query.category_id", "object.invalid_id")]});
         }

         const {skip, page, limit} = res.locals.pagination;

         let problems = await Problem
            .find(searchQuery)
            .sort({created_at: -1})
            .skip(skip)
            .limit(limit)
            .exec();

         let populated = new Array<any>();

         // Go trough all problems to assign requestor and fixer
         for (let problem of problems) {
            populated.push(await ProblemConstructor.Construct(problem));
         }

         res.json({data: populated, pagination: PaginationData.Generate(page, limit, (await Problem.find(searchQuery)).length)});

      } catch (error) {
         console.log(error);
         return res.status(400).json(ErrorHandeling.Handle(error));

      }
   }

   static async Store(req: Request, res: Response) {

      const schema = Joi.object({
         title: Joi.string().min(2).max(40).required(),
         description: Joi.string().min(10).max(200).required(),
         category_id: Joi.string().hex().length(24).required()
      });

      let validated = Validator.Validate(schema, req.body);

      if (validated !== true)
         return res.status(400).json(validated);

      let category = await Category.findOne({_id: req.body.category_id});

      if (category === undefined)
         return res.status(400).json({errors: [new ValidationError("Category does not exist!", "category_id", "object.null")]});

      try {
         let problem = await Problem.create({
            title: req.body.title,
            description: req.body.description,
            _requestor: res.locals.user._id,
            _category: req.body.category_id
         });

         res.status(201).json({data: await ProblemConstructor.Construct(problem)});

      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async Show(req: Request, res: Response) {
      const problem = res.locals.problem as IProblem;

      res.status(200).json({data: await ProblemConstructor.ConstructFull(problem)});
   }

   static async Update(req: Request, res: Response) {

      let problem = res.locals.problem as IProblem;

      const schema = Joi.object({
         title: Joi.string().min(2).max(40),
         description: Joi.string().min(10).max(200)
      });

      let validated = Validator.Validate(schema, req.body);

      if (validated !== true)
         return res.status(400).json(validated);

      if (req.body.title != undefined)
         problem.title = req.body.title
      if (req.body.description != undefined)
         problem.description = req.body.description

      try {
         await problem.save();
         return res.status(200).json({data: await ProblemConstructor.Construct(problem)});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async Destroy(req: Request, res: Response) {

      let problem = res.locals.problem as IProblem;

      try {
         await problem.remove();
         res.status(200).json({data: await ProblemConstructor.Construct(problem)});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async Assign(req: Request, res: Response) {
      let problem = res.locals.problem as IProblem;

      if (problem._fixer != null && problem._fixer != undefined)
         return res.status(400).json({error: "Can't assign to something that is taken"});

      problem._fixer = res.locals.user._id;

      try {
         await problem.save();
         res.status(200).json({data: await ProblemConstructor.Construct(problem)});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async RemoveAssign(req: Request, res: Response) {
      let problem = res.locals.problem as IProblem;

      if (problem._fixer != res.locals.user._id)
         return res.status(400).json({error: "Can't remove someone else assignment"});

      problem._fixer = undefined;

      try {
         await problem.save();
         res.status(200).json({data: await ProblemConstructor.Construct(problem)});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static Mark(status: "waiting" | "in progress" | "done") {

      return async (req: Request, res: Response) => {
         let problem = res.locals.problem as IProblem;

         if (problem._fixer != res.locals.user._id && problem._requestor != res.locals.user._id)
            return res.status(400).json({error: "Can't mark something that is taken"});

         problem.status = status;

         try {
            await problem.save();
            res.status(200).json({data: await ProblemConstructor.Construct(problem)});
         } catch (error) {
            return res.status(400).json(ErrorHandeling.Handle(error));
         }
      }

   }
}
