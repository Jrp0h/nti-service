import {Request, Response} from "express";

import Security from "../Utils/Security";

import User from "../Models/User";
import Token from "../Utils/Token";

import Joi from "joi";
import Validator from "../Utils/Validator";
import ErrorHandeling from "../Utils/ErrorHandeling";
import PaginationData from "../Utils/PaginationData";
import UserConstructor from "../Utils/UserConstructor";
import ValidationError from "../Errors/ValidationError";
import TokenModel from "../Models/TokenModel";

export default class UsersController {


   static async Login(req: Request, res: Response) {

      const {email, password} = req.body;

      let user = await User.findOne({email}).select("+password").exec();

      let errorObject = {
         error: "Invalid email and/or password"
      };

      // Email doesn't match
      if (user == undefined || user == null) {
         return res.status(401).json(errorObject);
      }

      // See if account is banned
      if (user.banned === true)
         return res.status(403).json({error: "Account has been banned"});

      // Password is incorrect
      if (!(await Security.ComparePassword(password, user.password))) {
         return res.status(401).json(errorObject);
      }

      const token = Token.Sign({
         user: {
            _id: user._id,
            email: user.email,
            name: user.name,
            permissions: user.permissions
         }
      });

      await TokenModel.create({
         _user_id: user._id,
         token,
      });

      return res.json({
         data: {
            token,
            type: "Bearer",
            expires: (new Date(Date.now() + 7 * 24 * 60 * 60 * 1000))
         }
      });
   }

   static async Register(req: Request, res: Response) {

      const schema = Joi.object({
         name: Joi.string().min(2).max(40).required(),
         email: Joi.string().email().regex(/ga.ntig.se$/).required(),
         password: Joi.string().min(8).max(50).required()
      });

      let validated = Validator.Validate(schema, req.body);

      if (validated !== true)
         return res.status(400).json(validated);

      try {
         await User.create({
            name: req.body.name,
            email: req.body.email,
            password: await Security.HashPassword(req.body.password)
         });
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }

      res.status(201).json({data: {message: "User successfully created"}});
   }

   static async Logout(req: Request, res: Response) {
      await TokenModel.deleteOne({_user_id: res.locals.user._id, token: res.locals.token});
      return res.status(200).json({data: {message: "User successfully logged out"}});
   }

   static async LogoutAll(req: Request, res: Response) {
      await TokenModel.deleteMany({_user_id: res.locals.user._id});
      return res.status(200).json({data: {message: "User successfully logged out from all instances"}});
   }

   static async Index(req: Request, res: Response) {
      const {skip, page, limit} = res.locals.pagination;

      let users = await User
         .find({permissions: 0})
         .sort({created_at: -1})
         .skip(skip)
         .limit(limit)
         .exec();

      let populated = new Array<any>();

      for (let user of users) {
         populated.push(UserConstructor.Construct(user));
      }

      return res.status(200).json({data: populated, pagination: PaginationData.Generate(page, limit, await User.countDocuments().exec())});
   }

   static Ban(shouldBan: boolean) {
      return async function (req: Request, res: Response) {

         if (req.params.id !== undefined) {
            if (!Validator.IsValidObjectId(<string>req.params.id))
               return res.status(400).json({errors: [new ValidationError("Id is invalid", "params.id", "object.invalid_id")]});
         }

         let u = await User.findOne({_id: req.params.id});

         if (u == undefined || u == null)
            return res.status(404).json({error: "User with id " + req.params.id + " not found"});

         try {
            u.banned = shouldBan;
            await u.save();

            if (shouldBan) TokenModel.deleteMany({_user_id: u._id});

            return res.status(200).json({data: {message: `User ${shouldBan == true ? "banned" : "unbanned"} successfully`}});
         } catch (error) {
            return res.status(400).json(ErrorHandeling.Handle(error));
         }
      }
   }
}
