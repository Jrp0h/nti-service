import {Request, Response} from "express";
import Joi from "joi";

import Comment, {IComment} from "../Models/Comment";
import {IProblem} from "../Models/Problem";
import CommentConstructor from "../Utils/CommentConstructor";
import ErrorHandeling from "../Utils/ErrorHandeling";
import Validator from "../Utils/Validator";

export default class CommentsController {

   static async Index(req: Request, res: Response) {

      let problem = res.locals.problem as IProblem;
      let onId = problem._id;

      if (res.locals.comment != undefined)
         onId = res.locals.comment._id;

      try {
         // Find all on this post
         let comments = await Comment
            .find({_on: onId})
            .sort({created_at: -1})
            .skip(res.locals.pagination.skip)
            .limit(res.locals.pagination.limit)
            .exec();

         let populated = new Array<any>();

         // Go trough all problems to assign requestor and fixer
         for (let comment of comments) {
            populated.push(await CommentConstructor.Construct(comment));
         }

         res.json({data: populated});

      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }

   }

   static async Store(req: Request, res: Response) {

      let problem = res.locals.problem as IProblem;
      let onId = problem._id;

      if (res.locals.comment != undefined)
         onId = res.locals.comment._id;

      const schema = Joi.object({
         body: Joi.string().min(2).max(40).required()
      });

      let validated = Validator.Validate(schema, req.body);

      if (validated !== true)
         return res.status(400).json(validated);

      try {
         let comment = await Comment.create({
            body: req.body.body,
            _poster: res.locals.user._id,
            _on: onId
         });

         res.status(201).json({data: await CommentConstructor.Construct(comment)});

      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }

   }

   static async Destroy(req: Request, res: Response) {

      let comment = res.locals.comment as IComment;

      if (comment._poster != res.locals.user._id)
         return res.status(401).json({error: "Unauthorized"});

      try {
         await comment.remove();
         res.status(200).json({data: await CommentConstructor.Construct(comment)});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }
}
