import express, {Request, Response} from "express";
import Joi from "joi";

import Category from "../Models/Category";
import ErrorHandeling from "../Utils/ErrorHandeling";
import PaginationData from "../Utils/PaginationData";
import Validator from "../Utils/Validator";

export default class CategoriesController {

   static async Index(req: Request, res: Response) {
      try {
         const {skip, page, limit} = res.locals.pagination;

         let categories = await Category
            .find()
            .sort({created_at: -1})
            .skip(skip)
            .limit(limit)
            .exec();

         let populated = new Array<any>();

         for (let category of categories) {
            populated.push({_id: category._id, name: category.name});
         }

         res.json({data: populated, pagination: PaginationData.Generate(page, limit, await Category.countDocuments().exec())});

      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async Store(req: Request, res: Response) {

      const schema = Joi.object({
         name: Joi.string().min(2).max(40).required()
      });

      let validated = Validator.Validate(schema, req.body);

      if (validated !== true)
         return res.status(400).json(validated);

      try {
         let category = await Category.create({
            name: req.body.name,
         });

         res.status(201).json({data: {_id: category._id, name: category.name}});

      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async Show(req: Request, res: Response) {

      try {
         let category = await Category.findOne({_id: req.params.id});

         if (category === undefined || category === null)
            return res.status(404).json({error: {status: 404, message: "Category with id " + req.params.id + " not found"}});

         return res.status(200).json({data: {_id: category._id, name: category.name}});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }

   static async Update(req: Request, res: Response) {
      const id = req.params.id;
      try {
         let category = await Category.findOne({_id: id});

         if (category === undefined || category === null)
            return res.status(404).json({error: {status: 404, message: "Category with id " + id + " not found"}});

         const schema = Joi.object({
            name: Joi.string().min(2).max(40).required()
         });

         let validated = Validator.Validate(schema, req.body);

         if (validated !== true)
            return res.status(400).json(validated);

         category.name = req.body.name;
         await category.save();

         return res.status(200).json({data: {_id: category._id, name: category.name}});
      } catch (error) {
         return res.status(400).json(ErrorHandeling.Handle(error));
      }
   }
}
