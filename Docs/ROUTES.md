# API

## General Information

### Authentication

When Auth is required, this header is expected:

```yaml
Authorization: <token type> <token value>
```

Marked as:

> Auth Required

Or (For admin-only routes)

> Admin Auth Required

### Metadata

created_at and updated_at exists on all objects

### Error

If returned object has key "error" or "errors", the request failed

### Pagination

All routes with pagination returns a pagination object along with data

They accept queries:

* limit
* page

Marked as:

> Paginated

Object is:

```json
"pagination": {
   "next": {
      "page": 3,
      "limit": 1
   },
   "previous": {
      "page": 1,
      "limit": 1
   },
   "start": {
      "page": 1,
      "limit": 1
   },
   "current": {
      "page": 2,
      "limit": 1
   },
   "end": {
      "page": 4,
      "limit": 1
   },
   "total_enteries": 4
}
```

## Routes

[Auth](./Routes/Auth.md)

[Problem](./Routes/Problem.md)

[Comment](./Routes/Comment.md)

[Category](./Routes/Category.md)
