# Comments

User must be admin or be the poster of the problem to access these routes

## Get comments from Problem

> Auth Required

```http
GET /api/problems/:id/comments
```

Returns:

```json
{
   "data": [
      {
         "_id": "<comment id>",
         "_on": "<problem id>",
         "body": "<comment body>",
         "created_at": "<date>",
         "updated_at": "<date>",
         "poster": {
            "_id": "<poster id>",
            "name": "<poster name>"
         },
         "comments": [
            "array of objects like this one but _on field is the comment aboves id"
         ]
      }
   ]
}
```

## Post comment to Problem

> Auth Required

```http
POST /api/problems/:id/comments
```

Expects:

```yaml
body: "string"
```

Returns:

```json
{
   "data": [
      "_id": "<comment id>",
      "_on": "<problem id>",
      "body": "<comment body>",
      "created_at": "<date>",
      "updated_at": "<date>",
      "poster": {
         "_id": "<poster id>",
         "name": "<poster name>"
      },
      "comments": [
         "array of objects like this one but _on field is the comment aboves id"
      ]
   ]
}
```

## Remove comment - both comment on problems and comments on comments

> Auth Required

```http
DELETE /api/problems/:id/comments
```

Returns:

```json
{
   "data": [
      "_id": "<comment id>",
      "_on": "<problem id>",
      "body": "<comment body>",
      "created_at": "<date>",
      "updated_at": "<date>",
      "poster": {
         "_id": "<poster id>",
         "name": "<poster name>"
      },
      "comments": [
         "array of objects like this one but _on field is the comment aboves id"
      ]
   ]
}
```

## Get comment and children

> Auth Required

```http
GET /api/problems/:problem_id/comments/:comment_id
```

Returns:

```json
{
   "data": [
      "_id": "<comment id>",
      "_on": "<problem id>",
      "body": "<comment body>",
      "created_at": "<date>",
      "updated_at": "<date>",
      "poster": {
         "_id": "<poster id>",
         "name": "<poster name>"
      },
      "comments": [
         "array of objects like this one but _on field is the comment aboves id"
      ]
   ]
}
```

## Post comment on comment

> Auth Required

```http
POST /api/problems/:id/comments/:comment_id
```

Expects:

```yaml
body: "string"
```

Returns:

```json
{
   "data": [
      "_id": "<comment id>",
      "_on": "<problem id>",
      "body": "<comment body>",
      "created_at": "<date>",
      "updated_at": "<date>",
      "poster": {
         "_id": "<poster id>",
         "name": "<poster name>"
      },
      "comments": [
         "array of objects like this one but _on field is the comment aboves id"
      ]
   ]
}
```
