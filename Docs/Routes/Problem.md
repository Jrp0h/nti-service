# Problems

## All Problems

> Admin Auth Required
>
> Paginated

```http
GET /api/problems/
```

Returns:

```json
{
   "data": [
      {
         "_id": "<problem id>",
         "title": "<problem name>",
         "description": "<problem description>",
         "status": "waiting",
         "created_at": "<date>",
         "updated_at": "<date>",
         "category": {
            "_id": "<category id>",
            "name": "<category name>"
         },
         "requestor": {
            "_id": "<users id>",
            "name": "<users name>"
         },
         "fixer": "same as requestor or null"
      }
   ]
}
```

## New Problem

> Auth Required

```http
POST /api/problems/
```

Expects:

```yaml
title: "string"
description: "string"
category_id: "category id"
```

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "title": "<problem name>",
      "description": "<problem description>",
      "status": "waiting",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": "same as requestor or null"
   }
}
```

## Specific Problem

> Auth Required

```http
GET /api/problems/:id
```

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "name": "<problem name>",
      "description": "<problem description>",
      "status": "waiting",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": {
         "_id": "<user id>",
         "name": "<user name>"
      }
   }
}
```

## Editing Problem

> Auth Required

```http
PATCH /api/problems/:id
```

Expects:

```yaml
title?: "string"
description?: "string"
```

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "name": "<problem name>",
      "description": "<problem description>",
      "status": "waiting",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": {
         "_id": "<user id>",
         "name": "<user name>"
      }
   }
}
```

## Deleting Problem

> Auth Required

```http
DELETE /api/problems/:id
```

Expects:

```yaml
title?: "string"
description?: "string"
```

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "name": "<problem name>",
      "description": "<problem description>",
      "status": "waiting",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": {
         "_id": "<user id>",
         "name": "<user name>"
      }
   }
}
```

## Assign fixer to Problem

> Admin Auth Required

```http
PATCH /api/problems/:id/assign
```

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "name": "<problem name>",
      "description": "<problem description>",
      "status": "waiting",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": {
         "_id": "<user id>",
         "name": "<user name>"
      }
   }
}
```

## Remove fixer from Problem

> Admin Auth Required

```http
DELETE /api/problems/:id/assign
```

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "name": "<problem name>",
      "description": "<problem description>",
      "status": "waiting",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": null
   }
}
```

## Set status on Problem

> Admin Auth Required

```http
PATCH /api/problems/:id/<status>
```

Statuses:

* waiting
* inprogress
* done

Returns:

```json
{
   "data": {
      "_id": "<problem id>",
      "name": "<problem name>",
      "description": "<problem description>",
      "status": "<new status>",
      "created_at": "<date>",
      "updated_at": "<date>",
      "category": {
         "_id": "<category id>",
         "name": "<category name>"
      },
      "requestor": {
         "_id": "<users id>",
         "name": "<users name>"
      },
      "fixer": null
   }
}
```
