# Category

## Get all Categories

> Auth Required

```http
GET /api/categories
```

Returns:

```json
{
   "data": [
      {
         "_id": "<comment id>",
         "name": "<category name>",
         "created_at": "<date>",
         "updated_at": "<date>"
      }
   ]
}
```

## Get Specific Category

> Auth Required

```http
GET /api/categories/:id
```

Returns:

```json
{
   "data": {
      "_id": "<comment id>",
      "name": "<category name>",
      "created_at": "<date>",
      "updated_at": "<date>"
   }
}
```

## Create new Category

> Admin Auth Required

```http
POST /api/categories
```

Expects:

```yaml
name: "string"
```

Returns:

```json
{
   "data": {
      "_id": "<comment id>",
      "name": "<category name>",
      "created_at": "<date>",
      "updated_at": "<date>"
   }
}
```

## Update Category

> Admin Auth Required

```http
POST /api/categories/:id
```

Expects:

```yaml
name: "string"
```

Returns:

```json
{
   "data": {
      "_id": "<comment id>",
      "name": "<category name>",
      "created_at": "<date>",
      "updated_at": "<date>"
   }
}
```
