# Auth

## Login

Endpoint

```http
POST /api/auth/login
```

Expects:

```yaml
email: "string"
password: "string"
```

Returns:

```json
{
   "data": {
      "token": "<api token>",
      "type": "Bearer",
      "expires": "<1 week>"
   }
}
```

## Register

Endpoint

```http
POST /api/auth/register
```

Expects:

```yaml
name: "string"
email: "string"
password: "string"
```

Returns:

```json
{
   "data": {
      "message": "User successfully created"
   }
}
```

## Logout

> Auth Required

Endpoint

```http
GET /api/auth/logout
```

Returns:

```json
{
   "data": {
      "message": "User successfully logged out"
   }
}
```

## Logout from all Instances

> Auth Required

Endpoint

```http
GET /api/auth/logout/all
```

Returns:

```json
{
   "data": {
      "message": "User successfully logged out from all instances"
   }
}
```

## Get all users

> Admin Auth Required
>
> Paginated

Endpoint

```http
GET /api/auth
```

Returns:

```json
{
   "data": [
      {
         "_id": "<user id>",
         "name": "<users name>",
         "permissions": "<user permissions>",
         "banned": "<user banned>",
         "email": "<users email>",
         "created_at": "<date>",
         "updated_at": "<date>"
      }
   ]
}
```

## Ban/unban user

> Admin Auth Required

Endpoint

```http
PATCH /api/auth/:user_id/<ban|unban>
```

Expects:

```yaml
nothing
```

Returns:

```json
{
   "data": {
      "message": "User banned/unbanned successfully"
   }
}
```
