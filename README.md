# Readme

MongoDB required, default port 27017
Server listens on default on port 5000

Setup and start:

* npm i
* npm i -g ts-node typescript
* Copy .env.example to .env
* ts-node src/main.ts

## Docs

[All routes can be found here](./Docs/ROUTES.md)

## Todo

### User

Change password, Forgot password, Email confirmation

When sign up, add to NeedVerify collection and send email verification,
when done add to normal Users collection

Log out everywhere(delete all tokens)

### Problem

Update Problems to use ENUM instead of string for status

Add filters, specially for done

### Error Handeling

Make more custom error and return correct HTTP Code.
