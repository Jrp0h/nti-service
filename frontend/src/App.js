import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { PropsRoute, PublicRoute, PrivateRoute } from 'react-router-with-props';
import { Container, Button, Alert, Row, Col } from "react-bootstrap";
import { AnimatePresence, motion } from "framer-motion";
import PropTypes from "prop-types";

// CSS
import "./css/bootstrap.min.css";
import "./css/master.css";

// Components
import Navigation from "./components/Navigation";
import Footer from "./components/Footer";

// Pages
import ErrorPage from "./pages/404";
import HomePage from "./pages/Home";

export default class App extends Component {
	render() {
		return (
			<Router>
                <Navigation />
                <AnimatePresence>
                    <Switch>
                        <Route exact path="/" component={HomePage} />
                        <Route component={HomePage} />
                    </Switch>
                </AnimatePresence>
                <Footer />
            </Router>
		);
	}
}