import React, { Component } from "react";
import { Carousel, Container, Alert, Navbar, Nav } from "react-bootstrap";
import { motion } from "framer-motion"

export default class Home extends Component {
    render() {
        return (
			<Navbar bg="dark" variant="dark">
				<Navbar.Brand href="#home">Navbar</Navbar.Brand>
					<Nav className="mr-auto">
					<Nav.Link href="#home">Home</Nav.Link>
					<Nav.Link href="#features">Features</Nav.Link>
					<Nav.Link href="#pricing">Pricing</Nav.Link>
				</Nav>
			</Navbar>
        );
    }
}
