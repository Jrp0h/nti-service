import React, { Component } from "react";
import { Carousel, Container, Alert } from "react-bootstrap";
import { motion } from "framer-motion"

export default class Home extends Component {
    render() {
        return (
            <motion.div className="app" animate={0} exit={1} initial={0}>
                <Alert variant="success">
					<Alert.Heading>Hey, nice to see you</Alert.Heading>
					<p>
						Aww yeah, you successfully read this important alert message. This example
						text is going to run a bit longer so that you can see how spacing within an
						alert works with this kind of content.
					</p>
					<hr />
					<p className="mb-0">
						Whenever you need to, be sure to use margin utilities to keep things nice
						and tidy.
					</p>
				</Alert>
            </motion.div>
        );
    }
}